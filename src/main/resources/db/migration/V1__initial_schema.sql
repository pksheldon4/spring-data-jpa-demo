CREATE TABLE if not exists CUSTOMER
(
    customer_id varchar(36),
    first_name  varchar(255),
    last_name   varchar(255),
    other_data  varchar(255),
    active      BOOLEAN default true
);