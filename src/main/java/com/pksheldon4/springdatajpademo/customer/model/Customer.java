package com.pksheldon4.springdatajpademo.customer.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Customer {

    @Id
    private String customerId;
    private String firstName;
    private String lastName;

    @Column(name="other_data")
    private String someOtherField;

    private boolean active = true;

    protected Customer() {
        customerId = UUID.randomUUID().toString();
    }

    public Customer(String firstName, String lastName) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
        this.someOtherField = "SomeOtherField";
    }
}