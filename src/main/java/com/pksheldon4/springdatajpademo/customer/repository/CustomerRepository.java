package com.pksheldon4.springdatajpademo.customer.repository;

import com.pksheldon4.springdatajpademo.customer.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, String> {

    List<Customer> findByLastName(String lastName);

    Customer findByCustomerId(String id);

    @Query(" from Customer c where c.firstName = :name or c.lastName = :name")
    List<Customer> findByLastNameOrFirstName(@Param("name") String name);

    @Modifying
    @Query("update Customer c set c.active = false where c.customerId = :customerId")
    void deactivateCustomerById(@Param("customerId") String customerId);

}
