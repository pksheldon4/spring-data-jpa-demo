package com.pksheldon4.springdatajpademo.customer.controller;

import com.pksheldon4.springdatajpademo.customer.model.Customer;
import com.pksheldon4.springdatajpademo.customer.repository.CustomerRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository repository) {
        customerRepository = repository;
    }

    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @GetMapping
    public List<Customer> customers() {
        return customerRepository.findAll();
    }

    @GetMapping("/delete")
    public void deleteCustomers() {
        customerRepository.deleteAll();
    }

}
