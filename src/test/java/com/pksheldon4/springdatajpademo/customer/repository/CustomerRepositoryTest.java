package com.pksheldon4.springdatajpademo.customer.repository;

import com.pksheldon4.springdatajpademo.customer.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class CustomerRepositoryTest {

    private static final String TEST_CUSTOMER_ID = "80b70b9c-2879-11eb-adc1-0242ac120002";
    private static final String TEST_FIRST_NAME = "Preston";
    private static final String TEST_LAST_NAME = "Sheldon";


    @Autowired
    private CustomerRepository repository;

    @BeforeEach
    @Sql(scripts = "/test-customer-cleanup.sql")
    public void init() {
        //running delete script
    }

    @Test
    @Sql(scripts = "/test-customer-data.sql")
    void findByLastName_returnsCustomer() {
        List<Customer> customers = repository.findByLastName(TEST_LAST_NAME);
        assertThat(customers).isNotNull();
        assertThat(customers).isNotEmpty();
        assertThat(customers.size()).isEqualTo(1);
        Customer customer = customers.get(0);
        assertThat(customer.getCustomerId()).isEqualTo(TEST_CUSTOMER_ID);

    }

    @Test
    @Sql(scripts = "/test-customer-data.sql")
    void findByCustomerId() {
        Customer customer = repository.findByCustomerId(TEST_CUSTOMER_ID);
        assertThat(customer).isNotNull();
        assertThat(customer.getFirstName()).isEqualTo(TEST_FIRST_NAME);
        assertThat(customer.getLastName()).isEqualTo(TEST_LAST_NAME);
    }

    @Test
    @Sql(scripts = "/test-customer-data.sql")
    void findByLastNameOrFirstName() {
        List<Customer> customers = repository.findByLastNameOrFirstName(TEST_FIRST_NAME);
        assertThat(customers).isNotNull();
        assertThat(customers).isNotEmpty();
        assertThat(customers.size()).isEqualTo(1);
        Customer customer = customers.get(0);
        assertThat(customer.getCustomerId()).isEqualTo(TEST_CUSTOMER_ID);
    }

    @Test
    @Sql(scripts = "/test-customer-data.sql")
    void deactivateCustomerById() {
        repository.deactivateCustomerById(TEST_CUSTOMER_ID);
        Customer customer = repository.findByCustomerId(TEST_CUSTOMER_ID);
        assertThat(customer).isNotNull();
        assertThat(customer.isActive()).isFalse();
    }
}