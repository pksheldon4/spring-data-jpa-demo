package com.pksheldon4.springdatajpademo.customer.repository;

import com.pksheldon4.springdatajpademo.customer.controller.CustomerController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("mysql")
class CustomerControllerIT {

    @Autowired
    private CustomerRepository customerRepository;
    private MockMvc mvc;

    @BeforeEach
    @Sql(scripts = "/test-customer-cleanup.sql")
    public void init() {
        mvc = MockMvcBuilders.standaloneSetup(new CustomerController(customerRepository)).build();
    }

    @Test
    void addCustomer(@Value("classpath:test-customer.json") Resource testCustomerResource) throws Exception {

        mvc.perform(post("/customers")
            .content(testCustomerResource.getInputStream().readAllBytes())
            .accept(MediaType.APPLICATION_JSON) //Response Type
            .contentType(MediaType.APPLICATION_JSON)) //Request Type
            .andExpect(status().isOk())
            .andDo(print()) //prints out request/response information. Useful in debugging.
            .andExpect(jsonPath("$.firstName", is("Test")))
            .andExpect(jsonPath("$.lastName", is("Customer")));
    }
}
