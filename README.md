# Spring Data JPA Demo

This application demonstrates developing an application that uses `spring-data-jpa` to simplify interactions with a relational database.

As with all SpringData implementations, you can get CRUD functionality up and running with 4 easy steps. 

1. Include the dependency in your pom.xml (Assumes this is a Spring Boot Application)
```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
```
2. Add the `@EnableJpaRepositories` annotation to your Main application class. This will tell SpringBoot to look for repository classes.
```java
@SpringBootApplication
@EnableJpaRepositories
public class SpringDataJpaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaDemoApplication.class, args);
    }
}
```

3. Extend one of the JPA Repository interfaces, specifying the data object that maps to the MongoDB repository, and the key data type. This provides significant default behavior. Note that you do not need to define any interface methods as the default behavior includes the standard CRUD operations as well as the ability to provide Pageable and Sort objects to influence the results of the queries.
```java
public interface CustomerRepository extends JpaRepository<Customer, String> {

}
```
4. Create the database connection information in application.yml or application.properties. 
```yaml
spring:
  datasource:
    url: jdbc:postgresql://${POSTGRES_HOST:localhost}:${POSTGRES_PORT:5432}/${POSTGRES_DB:postgres}
    username: ${POSTGRES_USERNAME:postgres}
    password: ${POSTGRES_PASSWORD:postgres}
    driver-class-name: org.postgresql.Driver
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQL10Dialect
```
This sample application can use a PostgreDB or MySqlDB by simply changing the value `SPRING_PROFILES_ACTIVE` since the DB specific configuration is contained in `application-postgres.yml` and `application-mysql.yml`

The above yaml snippet shows how you can use environment variables to override these properties while still providing meaningful default values.


## Running the Application Locally
The application includes a REST controller which manages CRUD operations for a Customer entity. There is no "business-logic" in this demo application, the REST Controller simply passes the requests to the Repository to interact with the Relational Database.

### Docker ###
The root of the project includes a `docker-compose.yml` file for local testing with docker.
The `docker-compose.yml` will create `postgres` and `mysql` containers and mount a local volumes so that your data will be persisted across re-starts.

You can run the application locally by executing the following commands from the root directory.
```shell script
docker-compose up -d
```
and then executing the spring-boot maven target
```shell script
mvn spring-boot:run
```

Once the application is running, you can use the `/customers` endpoint for CRUD operations on the database.

You can then read all customers using the following curl command.

```shell script
  curl http://localhost:8080/customers
```

And write a new customer using this curl command

```shell script
  curl --header "Content-Type: application/json" \
        --request POST \
        --data '{"firstName":"John","lastName":"Guthrie","someOtherField":null,"active":true}' \
        http://localhost:8080/customers
```

## Deploying the application and a PostgreDB to Kubernetes(K8s)

the ./k8s folder contains the following kubernetes artifacts.

1. `01-postgres-secret.yaml` - contains a K8s `Secret` containing the username/password of the Postgres server in encrypted form.
1. `02-postgres-config.yaml` - contains a K8s `ConfigMap` with environment variables identifying the PostgresDB server. (minus security info)
1. `03-postgres-statefulset.yaml` - contains the `StatefulSet` definition of the PostgresDB.
1. `04-postgres-demo.yaml` - contains the `Deployment` of the `postgres-demo` application.\*\*

None of the files have a namespace specified so unless you're deploying them to the default namespace, you need to either specify the namespace when deploying the artifact using `kubectl -n <namespace> apply -f <filename>`,
or execute `kubectl config set-context --current --namespace <namesapce>` prior to deploying any of the files using `kubectl apply -f <filename>`.

The `04-postgres-demo.yaml` references generic image, `spring-data-jpa-demo:latest` and will need to be updated with the location of the local image repository. For example `harbor.pksbeachhouse.com/library/spring-data-jpa-demo:latest`.
The deployment also includes a reference to `harbor-creds`, which is a `docker-registry-secret` containing the login information for the harbor instance this image will be pulled from.

```yaml
spec:
  imagePullSecrets:
    - name: harbor-creds
  containers:
    - name: postgres-demo
```

The creation of this k8s secret is a one-time operation for the namespace which is why it's not included in this repo. It's created using the following `kubectl` command.

```shell script
  kubectl create secret docker-registry harbor-creds --docker-username=[username] --docker-password='[password]' --docker-email=someone@gmail.
```

Both the Demo application and the PostgresDB instance take the username/password from the postgres-secret generated from the `postgres-secret.yaml` file. You can re-create this file with different values for username/password using the below command.

```shell script
  kubectl create secret generic postgres-secret --from-literal=username=[username] --from-literal=password=[password] \
        -o yaml --dry-run=client > 01-postgres-secret.yaml
```

## Notes on Testing

This application demonstrates a couple of different patterns for testing REST Controllers and Repositories.

It's considered a best-practice to limit the Spring configuration to only what is required. For example, even though this test is an integration test, testing from the controller through to the database, it doesn't use the `@SpringBootTest` annotation. This test instead uses the `@DataJpaTest` annotation. This creates a local database by default, however this test class overrides that using the `@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)` annotation. Therefore, in order for this test to work, a `mysql` database must be available.

```java
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("mysql")
class CustomerControllerIT {
    //code
}
```

This Test also demonstrates the ability to run Sql Scripts as part of your test

```java
    @BeforeEach
    @Sql(scripts = "/test-customer-cleanup.sql")
    public void init() {
       //code
    }
```